﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SbParser;

namespace SbParserTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var parser = new Parser();
            var ass = await parser.Parse("kill-them-all-worm-gamer.830187");

            Console.WriteLine("Hello World!");
            Console.WriteLine(ass.Title);
            Console.WriteLine(ass.ThreadMarks
                .OrderBy(tm => tm.PublishDate)
                .Aggregate("", (acc, tm) => 
@$"{acc}[{tm.Category}] {tm.Title} - {tm.WordCount} Words {tm.PublishDate.ToLongDateString()}
{tm.Url}
"
                ));
        }
    }
}
