﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SbRss.Services;

namespace SbRss.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RssController : ControllerBase
    {
        private readonly RssService _rssService;

        public RssController(RssService rssService)
        {
            this._rssService = rssService;
        }

        [ResponseCache(Duration = 30*60, VaryByQueryKeys = new[] {"url"})]
        [HttpGet]
        public async Task<IActionResult> Get(Uri url)
        {
            if (!url.IsAbsoluteUri)
            {
                return BadRequest();
            }

            var feed = await _rssService.GetRss(url);

            return File(feed, "application/rss+xml; charset=utf-8");
        }
    }
}