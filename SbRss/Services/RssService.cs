﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using SbParser;

namespace SbRss.Services
{
    public class RssService
    {
        private readonly Regex _tIdExtract = new Regex(@"^/threads/([^/]+)");

        public async Task<byte[]> GetRss(Uri url)
        {
            var parser = new Parser();
            var threadId = _tIdExtract.Match(url.LocalPath).Groups[1].Value;
            var res = await parser.Parse(threadId);
            var lastUpdated = res.ThreadMarks.Max(mark => mark.PublishDate);
            var feed = new SyndicationFeed(res.Title, res.Title, url, "RSSUrl", lastUpdated)
            {
                Copyright = new TextSyndicationContent($"{DateTime.Now.Year} SpaceBattles.com translated by Al"),
                Language = "en"
            };

            var items = res.ThreadMarks
                .OrderByDescending(tm => tm.PublishDate)
                .Take(10)
                .Select(ThreadmarkToItem);
            feed.Items = items;
            return RenderRss(feed);
        }

        private byte[] RenderRss(SyndicationFeed feed)
        {
            var settings = new XmlWriterSettings
            {
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Entitize,
                NewLineOnAttributes = true,
                Indent = true
            };
            using var stream = new MemoryStream();
            using var xmlWriter = XmlWriter.Create(stream, settings);
            var rssFormatter = new Rss20FeedFormatter(feed, false);
            rssFormatter.WriteTo(xmlWriter);
            xmlWriter.Flush();
            return stream.ToArray();
        }

        private static SyndicationItem ThreadmarkToItem(ThreadMark threadMark)
        {
            return new SyndicationItem($"[{threadMark.Category}] {threadMark.Title}", "", new Uri(threadMark.Url),
                threadMark.Url, threadMark.PublishDate);
        }
    }
}