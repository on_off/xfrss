﻿using System.Collections.Generic;

namespace SbParser
{
    public class SbThread
    {
        public string Title { get; set; }
        public ICollection<ThreadMark> ThreadMarks { get; set; }
    }
}