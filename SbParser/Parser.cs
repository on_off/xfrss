﻿using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SbParser
{
    public class Parser
    {
        private readonly IBrowsingContext _context;
        private const string LinksSelector = ".block-tabHeader--threadmarkCategoryTabs > span a";
        private const string ActiveHeaderSelector = LinksSelector + ".is-active";
        private const string TitleSelector = ".p-breadcrumbs > li:last-child a";

        public Parser()
        {
            var config = Configuration.Default.WithDefaultLoader();
            _context = BrowsingContext.New(config);
        }

        public async Task<SbThread> Parse(string threadId)
        {
            var address = $"https://forum.spacebattles.com/threads/{threadId}/threadmarks";
            var document = await _context.OpenAsync(address);
            var title = document.QuerySelector<IHtmlAnchorElement>(TitleSelector).Text.Trim();
            var anchors = document
                .QuerySelectorAll<IHtmlAnchorElement>(LinksSelector);
            // .Skip(1);
            var links = anchors
                .Select(link => _context.OpenAsync(link.Href))
                .Select(ProcessDocument);
            var marks = (await Task.WhenAll(links)).SelectMany(a => a);

            return new SbThread
            {
                Title = title,
                ThreadMarks = marks.ToList()
            };
        }

        public async Task<string> ParseThreadmarkPreview(ThreadMark mark, int symbolLimit = 255)
        {
            var document = await _context.OpenAsync($"https://forum.spacebattles.com{mark.PreviewUrl}");
            return document
                .QuerySelector(".threadmarkPreview .bbWrapper")
                .TextContent
                .Substring(0, symbolLimit);
        }

        private static async Task<IEnumerable<ThreadMark>> ProcessDocument(Task<IDocument> documentTask)
        {
            var document = await documentTask;
            return ParsePage(document);
        }

        private static IEnumerable<ThreadMark> ParsePage(IDocument page)
        {
            var header = page.QuerySelector(ActiveHeaderSelector).TextContent;
            var res = page
                .QuerySelectorAll<IHtmlDivElement>(".block-body--threadmarkBody div.structItem--threadmark")
                .Select(m => ProcessThreadMark(m, header));
            return res;
        }

        private static ThreadMark ProcessThreadMark(IHtmlDivElement markElement, string header)
        {
            var link = markElement.QuerySelector<IHtmlAnchorElement>(".structItem-cell--main a");
            var wordCount = markElement.QuerySelector(".structItem-cell--meta dd")?.TextContent;
            var publishDate = DateTime.Parse(markElement.QuerySelector<IHtmlTimeElement>("time").DateTime);
            var previewUrl = link.Attributes.GetNamedItem("data-preview-url")?.Value;
            return new ThreadMark
            {
                Title = link.Text,
                Category = header,
                WordCount = wordCount,
                PublishDate = publishDate,
                Url = link.Href,
                PreviewUrl = previewUrl
            };
        }
    }
}