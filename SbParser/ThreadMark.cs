﻿using System;

namespace SbParser
{
    public class ThreadMark
    {
        public string Title { get; set; }
        public DateTime PublishDate { get; set; }
        public string Category { get; set; }

        public string WordCount { get; set; }
        public string Url { get; set; }

        public string PreviewUrl { get; set; }
    }
}